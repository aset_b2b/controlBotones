var Main = {}; //construimos el objeto main que va a a ejecutar todas las instrucciones


//Esta es la funcion que ejecuta al inicar la aplicacion con ella definimos si el dispositivo tiene tizen o no, 
//si tiene tizen empieza entonces a ejecutar las funciones que vamos a crear 
Main.onLoad = function (){
	"use strict";
	 if (window.tizen === undefined) {
        
         setTimeout(
         		function(){
         			 Main.log('This application needs to be run on Tizen device');
         		},
         		1000
         );
     }else{
    	 Main.registAllKeys();  
         document.addEventListener('keydown', Main.onKeyDown, true); 
     }
};

//registramos todaS LAS TECLAS QUE VAMOS A USAR
//El nombre de las teclas y su respectivo codigo lo encontramos en http://www.samsungdforum.com/B2B/Guide/Development/SSSP4/tizen3551/index.html

Main.registAllKeys = function registerKeys() {
    var usedKeys = ['1','2','3','VolumeUp','VolumeDown','VolumeMute'];

    usedKeys.forEach(
        function (keyName) {
            tizen.tvinputdevice.registerKey(keyName);
        }
    );
};


//Esta funcion recibe los comando que entran de teclado y ya asi ejecuta cada una de las funciones que vamos programando

Main.onKeyDown = function () {
	document.addEventListener('keydown', function (e) {
        switch (e.keyCode) {
            case 49:
                Main.Hola();
                break;
            case 50:
                // Enter key
                Main.Adios();
                break;
            case 51:
                // Return key
                Main.BotonDos();
                break;
            case 447:
                // Return key
                Main.Grita();
                break;
            case 448:
                // Return key
                Main.Susurra();
                break;
            case 449:
                // Return key
                Main.Calla();
                break;
        }
    });
 
};

Main.Hola =  function() {  
	 document.getElementById('ejemplo').innerHTML = "ola k ace";
};

Main.Adios =  function() {  
	 document.getElementById('ejemplo').innerHTML = "Adios mundo cruel";
};

Main.BotonDos =  function() {  
	 document.getElementById('ejemplo').innerHTML = "Soy el boton dos ";
};

Main.Grita =  function() {  
	 document.getElementById('ejemplo').innerHTML = "Me estas gritando u.u'";
};

Main.Susurra =  function() {  
	 document.getElementById('ejemplo').innerHTML = "Habla mas fuerte o.O";
};

Main.Calla =  function() {  
	 document.getElementById('ejemplo').innerHTML = "Me callaste!!! >.< ";
};